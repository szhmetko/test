﻿using System;
using FogLampz.Model;

namespace FogBugzPd.Web.Controllers
{
	public static class MilestoneExtension
	{
		public static bool IsShared(this FixFor m)
		{
			return !m.IndexProject.HasValue;
		}

		public static DateTime? EndDate(this FixFor m)
		{
			return m.Date;
		}
	}
}