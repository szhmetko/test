﻿using FogBugzPd.Core;
using System.Web.Mvc;
using FogBugzPd.Web.Utils;
using StackExchange.Profiling;

namespace FogBugzPd.Web.Controllers
{
	public class ControllerBase : Controller
	{
		protected FogBugzPdDbContext DbContext
		{
			get { return FogBugzPdDbContext.Current; }
		}

		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			//Session is available at this point, so check if MiniProfiler should be visible
			if (EnvironmentUtils.GetEnvType() == EnvironmentType.Prod /*&& !UserContext.IsMiniProfilerEnabled*/)
				MiniProfiler.Stop(discardResults: true);

			base.OnActionExecuting(filterContext);
		}

	}
}