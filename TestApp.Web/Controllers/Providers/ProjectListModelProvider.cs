﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using FogBugzPd.Core.FogBugzApi;
using FogBugzPd.Web.Controllers.Interfaces;
using FogBugzPd.Web.Models.Project;
using FogLampz.Model;

namespace FogBugzPd.Web.Controllers.Providers
{
	public enum UserStatus
	{
		VeryBehind,
		LittleBehind,
		OnTime,
		LittleAhead,
		VeryAhead
	}

	public static class UserStatusExtensions
	{
		public static UserStatus ToStatusCode(this decimal val)
		{

			UserStatus status = UserStatus.OnTime;

			if (val < -3)
				status = UserStatus.VeryBehind;

			if (val < 0 && val >= -3)
				status = UserStatus.LittleBehind;

			if (val < 3 && val >= 0)
				status = UserStatus.OnTime;

			if (val < 5 && val >= 3)
				status = UserStatus.LittleAhead;

			if (val > 5)
				status = UserStatus.VeryAhead;

			return status;
		}
	}

	public class UserProjectStatusModel
	{
		public string UserName { get; set; }
		public decimal RemainingDays { get; set; }
		public decimal WorkedDays { get; set; }

		public decimal Status { get; set; }
		public decimal ActualStatus { get; set; }
		public decimal AdjustedStatus { get; set; }

	}

	public class ProjectStatusModel : ViewModelBase
	{
		public DashboardViewModel.DatesSectionViewModel DatesSection { get; set; }

		public IEnumerable<UserProjectStatusModel> UsersStatus { get; set; }

		public ProjectStatusModel()
		{
			DatesSection = new DashboardViewModel.DatesSectionViewModel();
		}
	}

	public interface IProjectStatusCalculator
	{
		IEnumerable<UserProjectStatusModel> GetUsersStatus(IEnumerable<Person> employees);
	}

	public class ProjectStatusCalculator : IProjectStatusCalculator
	{
		public IEnumerable<UserProjectStatusModel> GetUsersStatus(IEnumerable<Person> employees)
		{
			throw new NotImplementedException();
		}
	}

	public class DummyProjectStatusCalculator : IProjectStatusCalculator
	{
		public IEnumerable<UserProjectStatusModel> GetUsersStatus(IEnumerable<Person> employees)
		{
			return new List<UserProjectStatusModel>
				{
					new UserProjectStatusModel
						{
							UserName = "Eric", 
							RemainingDays = 7, 
							Status = -2, 
							WorkedDays = 4, 
							AdjustedStatus = -2.5m, 
							ActualStatus = -5
						},
					new UserProjectStatusModel
						{
							UserName = "Boris", 
							RemainingDays = 2, 
							Status = 3, 
							WorkedDays = 5, 
							AdjustedStatus = 3, 
							ActualStatus = 3
						},
				};

		}
	}

	public class ProjectListModelProvider : IProjectListModelProvider
	{
		private const int CombinationsLimit = 20;

		private readonly IFogBugzClientProvider _clientProvider;
		private readonly ICachingProvider _cacheProvider;

		public ProjectListModelProvider(IFogBugzClientProvider clientProvider, ICachingProvider caching)
		{
			_clientProvider = clientProvider;
			_cacheProvider = caching;
		}

		public ProjectListModelProvider() : this(new FogBugzClientProvider(), new CachingProvider())
		{
		}
		
		public class Combination
		{
			public int? ProjectId { get; set; }
			public int? MilestoneId { get; set; }
			public bool HasCases { get; set; }
			public string CacheKey { get; set; }

			public DateTime? MilestoneEndDate { get; set; }

			public string MilestoneName { get; set; }
			public string ProjectName { get; set; }
		}

		public class ProcessedCombinationsResult
		{
			public List<Combination> ProcessedCombinations { get; set; }
			public int ThreadCount { get; set; }
		}


		private string CacheKey
		{
			get { return _cacheProvider.GetCacheKey(); }
		}

		public List<Combination> PrepareCombinations(IEnumerable<Project> projects, IList<FixFor> milestones)
		{

			var sharedMileStones = milestones.Where(milestone => MilestoneExtension.IsShared(milestone))
			                                 .ToList();


			var combinations = new List<Combination>();
			foreach (var project in projects)
			{
				Project prj = project;
				var projectMilestones = milestones.Where(m => m.IndexProject == prj.Index)
				                                  .ToList();

				projectMilestones.AddRange(sharedMileStones);

				combinations.AddRange(
					projectMilestones.Select(m => new Combination
						{
							MilestoneId = m.Index,
							ProjectId = prj.Index,
							MilestoneEndDate = m.EndDate(),
							ProjectName = prj.Name,
							MilestoneName = m.Name
							//CacheKey = MsCacheKey.GenCaseSetKey(projectId, m.Index)
						}));
			}

			return combinations;
		}

		public List<Combination> ProcessCombinations(List<Combination> combinations, int threadsCount)
		{
			var existingResult = _cacheProvider.Retrieve<ProcessedCombinationsResult>(CacheKey);
			
			List<Combination> existingCaseSet = null;
			if (existingResult != null && existingResult.ThreadCount == threadsCount)
			{
				existingCaseSet = existingResult.ProcessedCombinations;
			}
			else
			{
				_cacheProvider.Remove(CacheKey);
			}

			if (existingCaseSet != null)
				return existingCaseSet;

			// Use ConcurrentQueue to enable safe enqueueing from multiple threads. 
			var exceptions = new ConcurrentQueue<Exception>();

			var sw = new Stopwatch();
			sw.Start();

			var client = _clientProvider.GetClient();

			// Define a delegate that prints and returns the system tick count
			Func<object, int> action = (object obj) =>
				{

					Combination combo = obj as Combination;

					//var localWatch = new Stopwatch();
					//localWatch.Start();
					var cases = FogBugzGateway.GetCaseSetSafeThread(client,
																	combo.MilestoneId,
																	combo.ProjectId, combo.CacheKey, 1);

					combo.HasCases = cases.Cases.Any();

					//localWatch.Stop();

					return 0;
				};

			var lcts = new LimitedConcurrencyLevelTaskScheduler(threadsCount);
			var factory = new TaskFactory(lcts);

			var  tasks = new Task<int>[combinations.Count()];

			for (int i = 0; i < combinations.Count(); i++)
			{
				tasks[i] = factory.StartNew(action, combinations[i], TaskCreationOptions.LongRunning);
			}

			//Exceptions thrown by tasks will be propagated to the main thread 
			//while it waits for the tasks. The actual exceptions will be wrapped in AggregateException. 
			try
			{
				// Wait for all the tasks to finish.
				Task.WaitAll(tasks);

			}
			catch (AggregateException e)
			{
				throw;
			}

			/*Parallel.ForEach(combinations,
			                 //new ParallelOptions
			                 //	{
			                 //		MaxDegreeOfParallelism = cpuCount ?? Environment.ProcessorCount
			                 //	},
			                 combo =>
				                 {
					                 try
					                 {
						                 var cases = FogBugzGateway.GetCaseSetSafeThread(client,
						                                                                 combo.MilestoneId,
						                                                                 combo.ProjectId, combo.CacheKey, 1);

						                 combo.HasCases = cases.Cases.Any();
					                 }
					                 catch (Exception e)
					                 {
						                 exceptions.Enqueue(e);
					                 }
				                 });*/
			

			if (exceptions.Any())
			{
				throw new AggregateException(exceptions);
			}

			// return legal combinations
			var result = combinations.Where(c => c.HasCases) //combinations with at least one task
			                         //.Where(c => (c.MilestoneEndDate ?? DateTime.Now) > DateTime.Now) //moved to process method
			                         .OrderBy(c => c.MilestoneEndDate) //Sort by Milestone End Date in future
			                         .Take(CombinationsLimit) //Get top 20 
			                         .ToList();

			_cacheProvider.Store(CacheKey,
			                     new ProcessedCombinationsResult {ProcessedCombinations = result, ThreadCount = threadsCount},
			                     new TimeSpan(1, 0, 0));

			sw.Stop();

			var elapsed = sw.Elapsed;

			return result;
		}

		public ListViewModel Process(IList<Project> projects, IList<FixFor> milestones,
									 IList<Case> parentCases, int threadsCount)
		{
			milestones = milestones.Where(c => (c.EndDate() ?? DateTime.Now) > DateTime.Now).ToList();

			var combinations = PrepareCombinations(projects, milestones);

			combinations = ProcessCombinations(combinations, threadsCount);

			var model = new ListViewModel();

			var sharedMileStones = milestones.Where(milestone => milestone.IsShared())
			                                 .ToList();

			var validProjects = projects.Where(p => combinations.Any(c => c.ProjectId == p.Index));

			foreach (Project project in validProjects)
			{
				var projectListItem = new ProjectListItem
					{
						Project = project
					};

				var projectId = project.Index;

				var projectMilestones = milestones.Where(m => m.IndexProject == projectId)
				                                  .ToList();

				projectMilestones.AddRange(sharedMileStones);

				var validMilestones = projectMilestones
					.Where(m => combinations.Any(c => c.ProjectId == projectId && c.MilestoneId == m.Index));

				foreach (var milestone in validMilestones)
				{
					projectListItem.MilestoneListItems.Add(new MilestoneListItem
						{
							Milestone = milestone,
							SubProjectParentCases = parentCases.Where(
								c => c.IndexProject == projectId && c.IndexFixFor == milestone.Index)
															   .OrderBy(c => c.Title)// sort alphabetical
							                                   .ToList()

						});

				}

				model.Items.Add(projectListItem);
			}


			// sort projects
			// by min milestone end date. So first project will have milestone that ends the quickest. 
			model.Items = model.Items
			                   .OrderBy(p => p.MilestoneListItems.Min(i => i.Milestone.EndDate()))
			                   .ToList();

			// sort milestones
			// by end date
			model.Items.ForEach(item =>
				{
					item.MilestoneListItems = item.MilestoneListItems
					                              .OrderBy(m => m.Milestone.EndDate())
					                              .ToList();
				});

			return model;
		}
	}
}