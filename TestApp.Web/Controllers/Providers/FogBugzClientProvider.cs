﻿using FogBugzPd.Core;
using FogBugzPd.Web.Controllers.Interfaces;
using FogLampz;

namespace FogBugzPd.Web.Controllers.Providers
{
	public class FogBugzClientProvider : IFogBugzClientProvider
	{
		public FogBugzClientEx GetClient()
		{
			// get  client from cache
			string cacheKey = MsCacheKey.Gen(MsCacheDataType.FogBugz_ClientByFogBugzUrl, UserContext.FogBugzUrl);

			object tmpObj = null;
			MsCache.TryGet(cacheKey, ref tmpObj);

			return tmpObj as FogBugzClientEx;
		}
	}
}