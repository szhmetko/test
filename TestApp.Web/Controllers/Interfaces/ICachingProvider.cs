﻿using System;

namespace FogBugzPd.Web.Controllers.Interfaces
{
	public interface ICachingProvider
	{
		T Retrieve<T>(string key);
		void Store(string key, object value, TimeSpan? absoluteExpiration = null, TimeSpan? slidingExpiration = null);

		string GetCacheKey();

		void Remove(string key);
	}
}