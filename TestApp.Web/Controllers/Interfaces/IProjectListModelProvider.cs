﻿using System.Collections.Generic;
using FogBugzPd.Web.Models.Project;
using FogLampz.Model;

namespace FogBugzPd.Web.Controllers.Interfaces
{
	public interface IProjectListModelProvider
	{
		ListViewModel Process(IList<Project> projects, IList<FixFor> milestones,
							  IList<Case> parentCases, int threadsCount);
	}
}