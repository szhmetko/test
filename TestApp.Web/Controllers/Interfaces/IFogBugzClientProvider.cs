﻿using FogLampz;

namespace FogBugzPd.Web.Controllers.Interfaces
{
	public interface IFogBugzClientProvider
	{
		FogBugzClientEx GetClient();
	}
}