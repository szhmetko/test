﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading;
using FogBugzPd.Web.Controllers.Interfaces;
using FogBugzPd.Web.Controllers.Providers;
using FogLampz;
using Moq;
using NUnit.Framework;

namespace FogBugzPd.Web.Tests
{

	namespace FogBugzPd.Core.Tests
	{
		public class CachingProviderStub : ICachingProvider
		{
			public T Retrieve<T>(string key)
			{
				return default(T);
			}

			public void Store(string key, object value, TimeSpan? absoluteExpiration, TimeSpan? slidingExpiration)
			{
			}

			public string GetCacheKey()
			{
				return "";
			}

			public void Remove(string key)
			{
			}
		}

		public class FogBugzClientProviderStub : IFogBugzClientProvider
		{
			private FogBugzClientEx Client { get; set; }

			public FogBugzClientProviderStub(FogBugzClientEx client)
			{
				Client = client;
			}

			public FogBugzClientEx GetClient()
			{
				return Client;
			}
		}

		[TestFixture]
		public class ProjectListModelProviderTest
		{
			private Mock<ICachingProvider> _cachingProvider;
			private Mock<IFogBugzClientProvider> _clientProvider;

			[SetUp]
			public void Setup()
			{

				Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
				Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

				_fogBugzClient = new FogBugzClientEx();

				_clientProvider = new Mock<IFogBugzClientProvider>();
				_clientProvider.Setup(p => p.GetClient())
				               .Returns(_fogBugzClient);

				_cachingProvider = new Mock<ICachingProvider>();
				_cachingProvider.Setup(p => p.Store(It.IsAny<string>(), It.IsAny<object>(), It.IsAny<TimeSpan?>(), It.IsAny<TimeSpan?>()))
				                .Callback(() => { });
				_cachingProvider.Setup(p => p.Retrieve<object>(It.IsAny<string>()))
				                .Returns(null);

				DoLogin(Login);
			}

			private class Credentials
			{
				public string UserName { get; set; }
				public string Password { get; set; }
				public string Url { get; set; }
			}

			private Credentials DemoLogin
			{
				get
				{
					return new Credentials
					{
						UserName = "igor@entechsolutions.com",
						Password = "igor12",
						Url = "https://fogbugzpm-demo.fogbugz.com"
					};
				}
			}

			private Credentials Login
			{
				get
				{
					return new Credentials
					{
						UserName = "finalbuilder@gramercyone.com",
						Password = "FogBugzKiw1",
						Url = "https://gramercyone.fogbugz.com"
					};
				}
			}

			private FogBugzClientEx _fogBugzClient;



			[Test, MTAThread]
			public void CanProcessCombinations()
			{
				var sw = new Stopwatch();
				sw.Start();

				var projects = _fogBugzClient.GetProjects();
				var  milestones = _fogBugzClient.GetFixFors().ToList();

				ProjectListModelProvider prv = new ProjectListModelProvider(new FogBugzClientProviderStub(_fogBugzClient), new CachingProviderStub());

				var combi = prv.PrepareCombinations(projects, milestones);

				prv.ProcessCombinations(combi/*.Take(100)*/.ToList(), combi.Count() / 2);

				sw.Stop();

				Console.WriteLine(sw.Elapsed);
			}

			private void DoLogin(Credentials credentials)
			{
				_fogBugzClient.LogOn(credentials.Url + "/api.asp", credentials.UserName, credentials.Password);
			}
		}
	}

}
